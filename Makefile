PREFIX ?= /usr/local
DESTDIR ?=

%.gz:man/%
	gzip -9 -c $< > $@

build:journald-ai.1.gz ai-cronjob.8.gz

install:build
	install -d ${DESTDIR}${PREFIX}/share/man/man1
	install -m 644 journald-ai.1.gz ${DESTDIR}${PREFIX}/share/man/man1
	install -d ${DESTDIR}${PREFIX}/share/man/man8
	install -m 644 ai-cronjob.8.gz ${DESTDIR}${PREFIX}/share/man/man8
	install -d ${DESTDIR}${PREFIX}/bin
	install -m 755 bin/journald-ai ${DESTDIR}${PREFIX}/bin
	install -d ${DESTDIR}${PREFIX}/lib/systemd
	install -m 644 lib/ai.service ${DESTDIR}${PREFIX}/lib/systemd
	install -d ${DESTDIR}${PREFIX}/lib/journald-ai
	install -m 755 lib/ai-cronjob ${DESTDIR}${PREFIX}/lib/journald-ai

clean:
	rm -f journald-ai.1.gz ai-cronjob.8.gz

.PHONY: build install clean

